package com.example.first_app;

import android.os.Bundle;
import android.os.Environment;

import io.flutter.app.FlutterActivity;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugins.GeneratedPluginRegistrant;

public class MainActivity extends FlutterActivity {
  private static final String CHANNEL = "downloads_path_provider";
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    GeneratedPluginRegistrant.registerWith(this);


    new MethodChannel(getFlutterView(), CHANNEL).setMethodCallHandler(
      new MethodChannel.MethodCallHandler(){

        public void onMethodCall(MethodCall call, MethodChannel.Result result){
          if (call.method.equals("getDownloadsDirectory")) {
            result.success(getDownloadsDirectory());
        } else {
            result.notImplemented();
        }
        }

      }
    );
  }





    // public class DownloadsPathProviderPlugin implements MethodCallHandler {
      /**
       * Plugin registration.
      //  */
      // public static void registerWith(Registrar registrar) {
      //     final MethodChannel channel = new MethodChannel(registrar.messenger(), "downloads_path_provider");
      //     channel.setMethodCallHandler(new DownloadsPathProviderPlugin());
      // }
  
      
      // }
  
  // }
  private String getDownloadsDirectory() {
    return Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).getAbsolutePath();


  }
}
