import 'package:first_app/models/bail.dart';

import './detailBailContent.dart';
import './detailBailAppBar.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailBail extends StatefulWidget {
  final Bail bail;

  DetailBail({Key key, this.bail}) : super(key: key);

  @override
  _DetailBailState createState() => _DetailBailState();
}

class _DetailBailState extends State<DetailBail> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: CustomScrollView(slivers: <Widget>[
    SliverPersistentHeader(
      delegate: DetailBailAppBar(
          expandedHeight: MediaQuery.of(context).size.height * 0.42,
          image: "assets/images/${widget.bail.photoId}",
          header: widget.bail.address),
      pinned: true,
    ),
     SliverList(
    delegate: SliverChildListDelegate([DetailBailContent(bail : widget.bail)])),
    ]));
  }
}
