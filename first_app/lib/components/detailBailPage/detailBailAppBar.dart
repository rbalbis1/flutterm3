import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailBailAppBar extends SliverPersistentHeaderDelegate {
  final double expandedHeight;
  final String image;
  final String header;
  final double EARLY_END = 90;

  DetailBailAppBar({@required this.expandedHeight, this.image, this.header});

  double getPourcentageScroll(
      double expandedHeight, double shrinkOffset, double earlyEnd) {
    final res = ((expandedHeight - earlyEnd) - (shrinkOffset)) /
        (expandedHeight - earlyEnd);
    if (res < 0)
      return 0;
    else
      return res;
  }

  double compt = 0;

  @override
  Widget build(
      BuildContext context, double shrinkOffset, bool overlapsContent) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.clip,
      children: [
       Image.asset(
            this.image,
            fit: BoxFit.cover,
            height: this.expandedHeight - 65 + 15,
          ),
        if (this.header != null)
          Positioned(
            bottom: 0,
            height: 45 +
                35 *
                    getPourcentageScroll(
                        expandedHeight, shrinkOffset, EARLY_END),
            width: MediaQuery.of(context).size.width,
            child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(15),
                    topRight: Radius.circular(15),
                  )),
              child: Stack(children: [
                Padding(
                  padding: EdgeInsets.fromLTRB(10, 5, 25, 5),
                  child: Align(
                    alignment: Alignment.centerLeft,
                    child: Text(
                      this.header,
                      textAlign: TextAlign.left,
                      style: TextStyle(
                          fontSize: 10 +
                              15 *
                                  getPourcentageScroll(
                                      expandedHeight, shrinkOffset, EARLY_END)),
                    ),
                  ),
                ),
                Positioned(
                  top: 0,
                  right: 0,
                  child: Padding(
                    padding: EdgeInsets.only(right: 5, top: 5),
                    child: GestureDetector(
                        onTap: () => {Navigator.pop(context)},
                        child: const Icon(Icons.clear)),
                  ),
                ),
              ]),
            ),
          ),
        Positioned(
          bottom: 0,
          child: Container(
            height: 1,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              color: Colors.black26,
            ),
          ),
        ),
      ],
    );
  }

  @override
  double get maxExtent => expandedHeight;

  @override
  double get minExtent => 100;

  @override
  bool shouldRebuild(SliverPersistentHeaderDelegate oldDelegate) {
    return true;
  }
}
