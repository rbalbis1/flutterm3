import 'package:first_app/Service/downloadPdf.dart';
import 'package:first_app/Service/pdfScreen.dart';
import 'package:first_app/models/bail.dart';
import 'package:first_app/models/bailDetail.dart';
import 'package:first_app/service/webService.dart';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DetailBailContent extends StatefulWidget {
  final Bail bail;

  DetailBailContent({Key key, this.bail}) : super(key: key);

  @override
  _MonAppBarCustomState createState() => _MonAppBarCustomState();
}

class _MonAppBarCustomState extends State<DetailBailContent> {

  BailDetail bailDetail;

  @override
  void initState() { 
    super.initState();
    WebService().getRequest("http://10.0.2.2:8080/apim3/baux?id="+ widget.bail.objectNumber, (json) => BailDetail.fromJson(json))
    .then((resp) => setState(() {bailDetail = resp;}));
  }

  Widget boutonDocument(String label, BuildContext context, String urlPDF) {
    return Padding(
        padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 5),
        child: Container(
          width: double.infinity,
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(5),
          ),
          child: FlatButton(
            onPressed: () => _launchURL(context, urlPDF),
            child: Text(
              label,
              style: TextStyle(color: Colors.white),
            ),
          ),
        ));
  }

  Widget pictoDescription(Image image, String label) {
    return Row(children: [
      Image(
        image: image.image,
        height: 24,
      ),
      Padding(
        padding: EdgeInsets.only(right: 10),
        child: Text(
          label,
          style: TextStyle(
            fontSize: 16,
            color: Color(0xffde4545),
          ),
        ),
      )
    ]);
  }

  String formatNumber(double number) {
    int numInt = number.toInt();
    String numString = numInt.toString();
    String res = numInt > 1000
        ? (numString.substring(0, numString.length - 3) + '\'')
        : '';
    res += numString.substring(numString.length - 3, numString.length - 1);
    res += (number - numInt).toString();
    return res;
  }

  void _launchURL(BuildContext context, String urlPDF) async {
    var pdf = DownloadPdf(url: urlPDF);

    final path = await pdf.doDownload();
    Navigator.push(
        context, MaterialPageRoute(builder: (context) => PDFScreen(path)));
  }

  Widget coutLoyer(String desc, String amount, [bool total = false]) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            desc,
            style: total
                ? Theme.of(context).textTheme.subhead
                : Theme.of(context).textTheme.body2,
          ),
          Text(
            "$amount CHF",
            style: total
                ? Theme.of(context).textTheme.subhead
                : Theme.of(context).textTheme.body2,
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
  //  print(bailDetail?.financialInfo?.fees);
    return Column(children: [
      Padding(
          padding: EdgeInsets.symmetric(horizontal: 10, vertical: 15),
          child: Row(children: [
            pictoDescription(Image.asset("assets/images/room.PNG"), "3 pièces"),
            pictoDescription(
                Image.asset("assets/images/stairs.PNG"), "Rez-de-chaussée"),
          ])),
      Padding(
        padding: const EdgeInsets.symmetric(horizontal: 10, vertical: 15),
        child: Container(
          decoration: BoxDecoration(
              color: Color(0xfff2f0f0),
              borderRadius: BorderRadius.all(Radius.circular(5))),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: [
                coutLoyer("Loyer",  bailDetail?.financialInfo?.rent),
                coutLoyer("Charges", bailDetail?.financialInfo?.fees),
                coutLoyer("Total",bailDetail?.financialInfo?.rent, true),
              ],
            ),
          ),
        ),
      ),
      Padding(
        padding: const EdgeInsets.all(8.0),
        child: Align(
          alignment: Alignment.centerLeft,
          child: Text(
            "DOCUMENTS",
            style: TextStyle(
              fontSize: 25,
              fontWeight: Theme.of(context).textTheme.subhead.fontWeight,
            ),
            textAlign: TextAlign.left,
          ),
        ),
      ),
      boutonDocument("Bouton 1", context,
          "https://toutimmo.ch/wp-content/uploads/2016/04/e_2016_04_18_807_d.pdf"),
      boutonDocument("Bouton 2", context,
          "https://toutimmo.ch/wp-content/uploads/2016/04/e_2016_04_18_807_d.pdf"),
      boutonDocument("Bouton 3", context,
          "https://toutimmo.ch/wp-content/uploads/2016/04/e_2016_04_18_807_d.pdf"),
      SizedBox(
        height: 800,
      ),
    ]);
  }
}
