import 'package:flutter/material.dart';

class HeaderAnimated extends StatefulWidget {
  final double expandedHeight;
  final String image;
  HeaderAnimated(
      {Key key, @required this.expandedHeight, @required this.image});

  @override
  _HeaderAnimatedState createState() => _HeaderAnimatedState();
}

class _HeaderAnimatedState extends State<HeaderAnimated> {
  double compt = 0.0;
  var expandedHeight;
  @override
  void initState() {
    expandedHeight = widget.expandedHeight;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onVerticalDragStart: (detail) {},
      onVerticalDragUpdate: (detail) {
       compt + detail.primaryDelta > 0 ? compt += detail.primaryDelta : 0;
        setState(() {});
        if (compt > 40) {
          setState(() {});
          Navigator.pop(context);
        }
      },
      onVerticalDragEnd: (detail) {
        if (compt > 40) {

        }
        else {compt = 0.0; setState(() {});}
      },
      child: Column(
        children: <Widget>[
          Container(
            width: double.infinity,
            height: compt,
            color: Colors.red,
          ),
         
        ],
      ),
    );
  }
}
