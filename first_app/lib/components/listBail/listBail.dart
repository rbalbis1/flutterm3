import 'package:first_app/Components/listBail/tileListBail.dart';
import 'package:first_app/Service/webService.dart';
import 'package:first_app/models/bail.dart';
import 'package:first_app/providers/bailProvider.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ListBail extends StatefulWidget {
  ListBail();

  @override
  _ListBailState createState() => _ListBailState();
}

class _ListBailState extends State<ListBail> {
  List<Bail> tableauBaux;
  var indexTab = 0;
  void _populatedBailList() {
    WebService<Bail>()
        .getRequest(
            "http://10.0.2.2:8080/apim3/baux", (json) => Bail.fromJson(json))
        .then((list) => {
              setState(() => {tableauBaux = list as List<Bail>})
            });
  }

  @override
  void initState() {
    super.initState();
     _populatedBailList();
  }

  @override
  Widget build(BuildContext context) {
    final tableauBaux = Provider.of<BailProvider>(context).items;
    var listPage = [
      '/listBail',
      '/profil',
    ];
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        title: Text(
          "Mon application Flutter",
          style: TextStyle(color: Colors.grey),
        ),
      ),
      body: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(left: 15.0, top: 15, right: 15),
            child: Text(
              "LOCATIONS",
              style: TextStyle(
                fontSize: 20,
                color: Colors.pinkAccent,
                fontWeight: FontWeight.w600,
              ),
              textAlign: TextAlign.left,
            ),
          ),
          if (tableauBaux != null)
            Expanded(
              child: ListView.builder(
                  itemCount: tableauBaux.length,
                  itemBuilder: (BuildContext context, int index) {
                    var bail = tableauBaux[index];
                    return TileListBail(key: UniqueKey(), bail: bail);
                  }),
            )
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
          onTap: (ct) {
            if (ct != indexTab) {
              Navigator.pushNamed(context, listPage[ct]);
            }
            setState(() {});
          },
          currentIndex: indexTab,
          items: [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              title: Text("Chez moi"),
            ),
            BottomNavigationBarItem(
                icon: Image.asset(
                  'assets/images/lifeline.jpg',
                  width: 24,
                ),
                title: Text("data")),
            BottomNavigationBarItem(icon: Icon(Icons.hotel), title: Text("ll")),
          ]),
    );
  }
}
