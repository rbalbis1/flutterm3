import 'package:first_app/Components/detailBailPage/detailBail.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class TileListBail extends StatelessWidget {
  final bail;

  const TileListBail({Key key, this.bail}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
                    padding:
                        const EdgeInsets.symmetric(vertical: 5, horizontal: 15),
                    child: GestureDetector(
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(builder: (context) => DetailBail(bail : bail)),
                      ),
                      child: Card(
                        semanticContainer: true,
                        clipBehavior: Clip.antiAliasWithSaveLayer,
                        child: Row(
                          //mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            if (bail.photoId != null)
                              Expanded(
                                  flex: 1,
                                  child: Image.asset(
                                    "assets/images/${bail.photoId}",
                                    height:80,
                                    fit: BoxFit.fill,
                                  )),
                            Expanded(
                              flex: 3,
                              child: Padding(
                                padding:
                                    const EdgeInsets.only(left: 15),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text(
                                      bail.category,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w400,
                                          color: Colors.black87),
                                      textAlign: TextAlign.left,
                                    ),
                                    Text(
                                      bail.address,
                                      style: TextStyle(
                                          fontWeight: FontWeight.w600),
                                      textAlign: TextAlign.left,
                                    ),
                                    Row(
                                      children: <Widget>[
                                        if (bail.rooms != null)
                                          Flexible(
                                              flex: 2,
                                              child: Text(
                                                '${bail.rooms} pièces -',
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey),
                                              )),
                                        if (bail.floor != null)
                                          Expanded(
                                              flex: 2,
                                              child: Text(
                                                ' ${bail.floor?.toString()}',
                                               maxLines: 1,
                                               overflow: TextOverflow.fade,
                                                style: TextStyle(
                                                    fontWeight: FontWeight.w300,
                                                    color: Colors.grey),
                                                textAlign: TextAlign.left,
                                              )),
                                      ],
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
  }
}