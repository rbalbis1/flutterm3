import 'dart:io';

import 'package:camera/camera.dart';
import 'package:first_app/components/profilPage/profilPage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';

Future<CameraDescription> init() async {
  final camera = await availableCameras();

  final firstCam = camera.first;

  return firstCam;
}

class PhotoPage extends StatefulWidget {
  CameraController _controller;
  Future<void> _initializeControllerFuture;
  @override
  _PhotoPageState createState() => _PhotoPageState();
}

class _PhotoPageState extends State<PhotoPage> {
  CameraDescription camera;
  @override
  void initState() {
    init().then((camdesc) {
      camera = camdesc;
      widget._controller = CameraController(camera, ResolutionPreset.high);
      widget._initializeControllerFuture = widget._controller.initialize();
      setState(() {});
    });
    super.initState();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    widget._controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: FutureBuilder<void>(
          future: widget._initializeControllerFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done) {
              var ca = CameraPreview(widget._controller);
              return ca;
            } else {
              return Center(
                child: CircularProgressIndicator(),
              );
            }
          },
        ),
        floatingActionButton: FloatingActionButton(
          child: Icon(Icons.camera_alt),
          // Provide an onPressed callback.
          onPressed: () async {
            // Take the Picture in a try / catch block. If anything goes wrong,
            // catch the error.
            try {
              // Ensure that the camera is initialized.
              // await widget._initializeControllerFuture;

              // Construct the path where the image should be saved using the
              // pattern package.
              final path = join(
                // Store the picture in the temp directory.
                // Find the temp directory using the `path_provider` plugin.
                (await getTemporaryDirectory()).path,
                '${DateTime.now()}.png',
              );

              // Attempt to take a picture and log where it's been saved.
              await widget._controller.takePicture(path);

              // If the picture was taken, display it on a new screen.
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => Scaffold(
                    body: SafeArea(
                        child: Column(
                      children: <Widget>[
                        Expanded(
                          flex: 6,
                          child: Image.file(
                            File(path),
                          ),
                        ),
                        Expanded(
                          flex: 1,
                          child: RaisedButton(

                          child: Icon(Icons.check),
                            onPressed: (){
                              return Navigator.of(context).pop(path);
                            }),
                        )
                      ],
                    )),
                  ),
                ),
              ).then((val) {
                Navigator.of(context).pop(val);});
            } catch (e) {
              // If an error occurs, log the error to the console.
              print(e);
            }
          },
        ));
  }
}
