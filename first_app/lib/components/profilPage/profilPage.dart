import 'dart:io';

import 'package:first_app/service/app_localization.dart';
import 'package:first_app/service/photoService.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

class ProfilPage extends StatefulWidget {
  final String imagePath;

  ProfilPage([this.imagePath]);
  @override
  _ProfilPageState createState() => _ProfilPageState();
}

Widget rowOfText(String label, String desc) {
  return Row(
    crossAxisAlignment: CrossAxisAlignment.end,
    children: <Widget>[
      Expanded(
        flex: 1,
        child: Padding(
            padding: const EdgeInsets.only(left: 15.0, top: 15),
            child: Text(
              label,
              style: TextStyle(fontWeight: FontWeight.w600),
            )),
      ),
      Expanded(
        flex: 2,
        child: Text(desc),
      )
    ],
  );
}

Widget editButton(BuildContext context, String label, Function fonction) {
  return Padding(
    padding: const EdgeInsets.symmetric(horizontal: 30.0, vertical: 5),
    child: SizedBox(
      width: double.infinity,
      height: 50,
      child: FlatButton(
        hoverColor: Colors.black87,
        child: Text(
          label,
          style: TextStyle(color: Colors.white),
        ),
        color: Theme.of(context).primaryColor,
        onPressed: () => fonction(),
      ),
    ),
  );
}

class _ProfilPageState extends State<ProfilPage> {
  ImageProvider profilPic;
  Color couleur;

  void initState() {
    if (widget.imagePath != null) {
      PhotoService.image = FileImage(File(widget.imagePath));
    } else if (PhotoService.image == null) {
      PhotoService.image = NetworkImage(
          "https://img2.thejournal.ie/inline/1881369/original/?width=630&version=1881369");
    }
    super.initState();
  }

  Future getImage(ImageSource source) async {
    var image = await ImagePicker.pickImage(source: source);

    setState(() {
     
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(children: <Widget>[
          AnimatedContainer(
            color: Colors.white,
            duration: Duration(minutes: 1),
            width: 100,
            height: 100,
          ),
          Container(
              height: 14,
              width: double.infinity,
              child: Align(
                  alignment: Alignment.topRight, child: Icon(Icons.close))),
          Padding(
            padding: const EdgeInsets.all(30.0),
            child: GestureDetector(
              onTap: () {
                showDialog(
                    context: context,
                    child: SimpleDialog(
                      title: const Text('Select assignment'),
                      children: <Widget>[
                        SimpleDialogOption(
                          onPressed: ()=> getImage(ImageSource.camera)
                          // () { 
                            // Navigator.pushReplacementNamed(context, '/photo')
                            //     .then((imagePath) {
                            //   if (imagePath != null)
                            //     PhotoService.image = FileImage(File(imagePath));
                            //   setState(() {});
                            // });
                         // },
                         ,
                          child: const Text('Treasury department'),
                        ),
                        SimpleDialogOption(
                          onPressed: ()=> getImage(ImageSource.gallery)
                          //   Navigator.pushReplacementNamed(context, '/photo')
                          //       .then((imagePath) {
                          //     if (imagePath != null)
                          //       PhotoService.image = FileImage(File(imagePath));
                          //     setState(() {});
                          //   });
                          // }
                          ,
                          child: const Text('State department'),
                        ),
                      ],
                    ));
              },
              child: Container(
                  width: 150,
                  height: 150,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      image: DecorationImage(
                          fit: BoxFit.cover, image: PhotoService.image))),
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 40),
            child: Column(
              children: <Widget>[
                rowOfText(AppLocalizations.of(context).translate("hello"), "Kondo"),
                rowOfText("Prénom", "Marie Anaru"),
                rowOfText("Adresse", "21, Route des Contamines 1207 Genève"),
                rowOfText("E-mail", "marie.kondo@gmail.com"),
                rowOfText("Téléphone", "079 455 55 55"),
              ],
            ),
          ),
          editButton(context, "Modifier mon profil", () {
            setState(() {
              couleur = Colors.blue;
            });
          }),
          // editButton(context, "Modifier mes coordonnées bancaires", () {

          // }),
        ]),
      ),
    );
  }
}
