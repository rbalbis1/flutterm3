import 'package:first_app/Components/detailBailPage/detailBail.dart';
import 'package:first_app/Components/listBail/listBail.dart';
import 'package:first_app/components/photoPage/photoPage.dart';
import 'package:first_app/components/profilPage/profilPage.dart';
import 'package:first_app/providers/bailProvider.dart';
import 'package:first_app/service/app_localization.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/provider.dart';

import 'package:flutter/material.dart';

import 'package:flutter/services.dart' show rootBundle;

void main() {
  runApp(MyApp());
}

Future<String> loadJsonFromAsset(language) async {
  return await rootBundle.loadString('assets/i18n/' + language + '.json');
}

class MyApp extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return MyState();
  }
}

class MyState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      create: (ctx) => BailProvider(),
      child: MaterialApp(
        home: ListBail(),
        routes: {
          '/listBail': (context) => ListBail(),
          '/detailBail': (context) => DetailBail(),
          '/profil': (context) => ProfilPage(),
          '/photo': (context) => PhotoPage(),
        },
        theme: ThemeData(
          primaryColor: Color(0xffF72A59),
          primaryColorLight: Color(0xffbfbdbd),
          primaryColorDark: Color(0xff7d7a7a),
          fontFamily: 'OpenSans',
          textTheme: TextTheme(
            headline: TextStyle(fontSize: 30),
            body1: TextStyle(fontSize: 16),
            body2: TextStyle(
                fontSize: 18,
                color: Color(0xff7d7a7a),
                fontWeight: FontWeight.w600),
            subhead: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
          ),
        ),
        supportedLocales: [
          Locale("fr", "FR"),
          Locale("en", "US"),
        ],
        localizationsDelegates: [
          AppLocalizations.delegate,
          GlobalMaterialLocalizations
              .delegate, // Permet de traduire automatique certain bouton comme sur les alertdialog avec le bouton annulé
          GlobalWidgetsLocalizations
              .delegate, // Change la direction du texte de droite a gauche en fonction de la langue
        ],
        localeResolutionCallback: (locale, supportedLocales) {
          // Check si la langue du device est supporté par l'application
          var currentLocale;
          for (var supportedLocale in supportedLocales) {
            if (supportedLocale.languageCode == locale.languageCode) {
              currentLocale = supportedLocales;
            }
            if (supportedLocale.countryCode == locale.countryCode) {
              return supportedLocale;
            }
          }
          // Si la langue n'est pas supporté on renvoie le premier locale de la liste
          if (currentLocale == null) {
            return supportedLocales.first;
          } else {
            return currentLocale;
          }
        },
      ),
    );
  }
}
