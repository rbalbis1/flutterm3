class Bail {
  String _leaseId;
  String _address;
  String _photoId;
  String _floor;
  int _rooms;
  int _surface;
  String _category;
  int _categoryType;
  String _objectNumber;

  Bail(
      {String leaseId,
      String address,
      String photoId,
      String floor,
      int rooms,
      int surface,
      String category,
      int categoryType,
      String objectNumber}) {
    this._leaseId = leaseId;
    this._address = address;
    this._photoId = photoId;
    this._floor = floor;
    this._rooms = rooms;
    this._surface = surface;
    this._category = category;
    this._categoryType = categoryType;
    this._objectNumber = objectNumber;
  }

  String get leaseId => _leaseId;
  set leaseId(String leaseId) => _leaseId = leaseId;
  String get address => _address;
  set address(String address) => _address = address;
  String get photoId => _photoId;
  set photoId(String photoId) => _photoId = photoId;
  String get floor => _floor;
  set floor(String floor) => _floor = floor;
  int get rooms => _rooms;
  set rooms(int rooms) => _rooms = rooms;
  int get surface => _surface;
  set surface(int surface) => _surface = surface;
  String get category => _category;
  set category(String category) => _category = category;
  int get categoryType => _categoryType;
  set categoryType(int categoryType) => _categoryType = categoryType;
  String get objectNumber => _objectNumber;
  set objectNumber(String objectNumber) => _objectNumber = objectNumber;

  factory  Bail.fromJson(Map<String, dynamic> json) {
    return Bail(leaseId: json['leaseId'],
    address : json['address'],
    photoId : json['photoId'],
    floor : json['floor'],
    rooms : json['rooms'],
    surface : json['surface'],
    category : json['category'],
    categoryType : json['categoryType'],
    objectNumber : json['objectNumber'],);
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['leaseId'] = this._leaseId;
    data['address'] = this._address;
    data['photoId'] = this._photoId;
    data['floor'] = this._floor;
    data['rooms'] = this._rooms;
    data['surface'] = this._surface;
    data['category'] = this._category;
    data['categoryType'] = this._categoryType;
    data['objectNumber'] = this._objectNumber;
    return data;
  }


}