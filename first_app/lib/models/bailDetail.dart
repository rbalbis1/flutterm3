class BailDetail {
  String _leaseId;
  String _address;
  String _leaseStartDate;
  String _photoId;
  String _floor;
  int _rooms;
  int _surface;
  String _category;
  String _categoryType;
  String _objectNumber;
  FinancialInfo _financialInfo;
  List<Documents> _documents;

  BailDetail(
      {String leaseId,
      String address,
      String leaseStartDate,
      String photoId,
      String floor,
      int rooms,
      int surface,
      String category,
      String categoryType,
      String objectNumber,
      FinancialInfo financialInfo,
      List<Documents> documents}) {
    this._leaseId = leaseId;
    this._address = address;
    this._leaseStartDate = leaseStartDate;
    this._photoId = photoId;
    this._floor = floor;
    this._rooms = rooms;
    this._surface = surface;
    this._category = category;
    this._categoryType = categoryType;
    this._objectNumber = objectNumber;
    this._financialInfo = financialInfo;
    this._documents = documents;
  }

  String get leaseId => _leaseId;
  set leaseId(String leaseId) => _leaseId = leaseId;
  String get address => _address;
  set address(String address) => _address = address;
  String get leaseStartDate => _leaseStartDate;
  set leaseStartDate(String leaseStartDate) => _leaseStartDate = leaseStartDate;
  String get photoId => _photoId;
  set photoId(String photoId) => _photoId = photoId;
  String get floor => _floor;
  set floor(String floor) => _floor = floor;
  int get rooms => _rooms;
  set rooms(int rooms) => _rooms = rooms;
  int get surface => _surface;
  set surface(int surface) => _surface = surface;
  String get category => _category;
  set category(String category) => _category = category;
  String get categoryType => _categoryType;
  set categoryType(String categoryType) => _categoryType = categoryType;
  String get objectNumber => _objectNumber;
  set objectNumber(String objectNumber) => _objectNumber = objectNumber;
  FinancialInfo get financialInfo => _financialInfo;
  set financialInfo(FinancialInfo financialInfo) =>
      _financialInfo = financialInfo;
  List<Documents> get documents => _documents;
  set documents(List<Documents> documents) => _documents = documents;

  BailDetail.fromJson(Map<String, dynamic> json) {
    _leaseId = json['leaseId'];
    _address = json['address'];
    _leaseStartDate = json['leaseStartDate'];
    _photoId = json['photoId'];
    _floor = json['floor'];
    _rooms = json['rooms'];
    _surface = json['surface'];
    _category = json['category'];
    _categoryType = json['categoryType'];
    _objectNumber = json['objectNumber'];
    _financialInfo = json['financialInfo'] != null
        ? new FinancialInfo.fromJson(json['financialInfo'])
        : null;
    if (json['documents'] != null) {
      _documents = new List<Documents>();
      json['documents'].forEach((v) {
        _documents.add(new Documents.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['leaseId'] = this._leaseId;
    data['address'] = this._address;
    data['leaseStartDate'] = this._leaseStartDate;
    data['photoId'] = this._photoId;
    data['floor'] = this._floor;
    data['rooms'] = this._rooms;
    data['surface'] = this._surface;
    data['category'] = this._category;
    data['categoryType'] = this._categoryType;
    data['objectNumber'] = this._objectNumber;
    if (this._financialInfo != null) {
      data['financialInfo'] = this._financialInfo.toJson();
    }
    if (this._documents != null) {
      data['documents'] = this._documents.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class FinancialInfo {
  String _rent;
  String _fees;

  FinancialInfo({String rent, String fees}) {
    this._rent = rent;
    this._fees = fees;
  }

  String get rent => _rent;
  set rent(String rent) => _rent = rent;
  String get fees => _fees;
  set fees(String fees) => _fees = fees;

  FinancialInfo.fromJson(Map<String, dynamic> json) {
    _rent = json['rent'];
    _fees = json['fees'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['rent'] = this._rent;
    data['fees'] = this._fees;
    return data;
  }
}

class Documents {
  String _docId;
  String _name;
  String _docType;

  Documents({String docId, String name, String docType}) {
    this._docId = docId;
    this._name = name;
    this._docType = docType;
  }

  String get docId => _docId;
  set docId(String docId) => _docId = docId;
  String get name => _name;
  set name(String name) => _name = name;
  String get docType => _docType;
  set docType(String docType) => _docType = docType;

  Documents.fromJson(Map<String, dynamic> json) {
    _docId = json['docId'];
    _name = json['name'];
    _docType = json['docType'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['docId'] = this._docId;
    data['name'] = this._name;
    data['docType'] = this._docType;
    return data;
  }
}
