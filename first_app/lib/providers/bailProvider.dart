
import 'package:first_app/Service/webService.dart';
import 'package:first_app/models/bail.dart';
import 'package:flutter/cupertino.dart';

class BailProvider with ChangeNotifier{

  BailProvider(){
    _populatedBailList();
  }

  List<Bail> _items = [];
  List<Bail> get items {
    return [..._items]; // Retourne une copie de la liste des items pour eviter de modifier les autres listeners
  }
  void addBail(Bail value) {
    _items.add(value);
    notifyListeners();
  }

  void setListBail(List<Bail> value){
    this._items = value;
    notifyListeners();
  }

   void _populatedBailList() {
    WebService<Bail>()
        .getRequestList(
            "http://10.0.2.2:8080/apim3/baux", (json) => Bail.fromJson(json))
        .then((list) => setListBail(list));
  }

  
}