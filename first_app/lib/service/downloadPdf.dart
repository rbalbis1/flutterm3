import 'dart:io';

import 'package:flutter/services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:http/http.dart' as http;


class DownloadsPathProvider {
  static const MethodChannel _channel =
      const MethodChannel('downloads_path_provider');

  static Future<Directory> get downloadsDirectory async {
    final String path = await _channel.invokeMethod('getDownloadsDirectory');
    if (path == null) {
      return null;
    }
    return Directory(path);
  }
}

class DownloadPdf {
  String _url;
  DownloadPdf({url}){
    this._url = url;
  }

  
  
  doDownload() async {

    // Recuperer le dossier qui contient les documents a enregistrer
    final directory = await getApplicationDocumentsDirectory();

    Directory downloadsDirectory = await DownloadsPathProvider.downloadsDirectory;
    

    // Ouvre le fichier teste.pdf dans le dossier des documents
    var _localFile = File('${downloadsDirectory.path}/meinteste3.pdf');

    // charge la page qui contient le pdf 
    final response = await http.get(this._url);

    // Recupere le fichier pdf sous forme de bytes
    final responseJson = response.bodyBytes;

    // Ecrit le pdf dans le fichier teste.pdf
    _localFile.writeAsBytes(responseJson);

  return _localFile.path;
  }
  
  
  }
