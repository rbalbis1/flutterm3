import 'dart:convert';

import "package:http/http.dart" as http;

class WebService<T> {
  Future<List<T>> getRequestList(String url, Function jsonParser) async {

    final response = await http.get(url);
    if (response.statusCode == 200) {
      List<T> res = [];
      for (var bail in jsonDecode(response.body)) {
        res.add(jsonParser(bail));
       
      }
      return res;
    }
    return null;
  }

  
 Future<T> getRequest(String url, Function jsonParser) async {

    final response = await http.get(url);
    if (response.statusCode == 200) {
      T res = jsonParser(jsonDecode(response?.body));
      return res;
    }
    return null;
  }
}
