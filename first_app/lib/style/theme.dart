import 'package:flutter/material.dart';

class MonTheme {
  static ThemeData get theme {
    return ThemeData(
        primaryColor: Color(0xffF72A59),
        primaryColorLight: Color(0xffbfbdbd),
        primaryColorDark: Color(0xff7d7a7a),
        fontFamily: 'OpenSans',
        textTheme: TextTheme(
          headline: TextStyle(fontSize: 30),
          body1: TextStyle(fontSize: 16),
          body2: TextStyle(
              fontSize: 18,
              color: Color(0xff7d7a7a),
              fontWeight: FontWeight.w600),
          subhead: TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
        ));
  }
}
